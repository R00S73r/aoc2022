from __future__ import annotations
from .lib import file_input
from typing import Union, Tuple

INPUT_NAME = "input2.txt"


class GameLogic:
    value: int
    win: Union[Rock, Paper, Scissors]
    draw: Union[Rock, Paper, Scissors]
    loose: Union[Rock, Paper, Scissors]

    def beats(self, against: Union[Rock, Paper, Scissors]) -> bool:
        return isinstance(against, type(self.win))

    def define(
        self,
        value: int,
        win: Union[Rock, Paper, Scissors],
        loose: Union[Rock, Paper, Scissors],
    ) -> None:
        self.value = value
        self.win = win
        self.draw = self
        self.loose = loose

    def __str__(self) -> str:
        return self.__class__.__name__


class Rock(GameLogic):
    ...


class Paper(GameLogic):
    ...


class Scissors(GameLogic):
    ...


rock = Rock()
paper = Paper()
scissors = Scissors()

rock.define(value=1, win=scissors, loose=paper)
paper.define(value=2, win=rock, loose=scissors)
scissors.define(value=3, win=paper, loose=rock)


POINTS = {"loose": 0, "draw": 3, "win": 6}


def game1(opponent: str, me: str) -> int:
    convert = {
        "A": rock,
        "X": rock,
        "B": paper,
        "Y": paper,
        "C": scissors,
        "Z": scissors,
    }
    if isinstance(convert[me], type(convert[opponent])):
        return POINTS["draw"] + convert[me].value
    if convert[me].beats(convert[opponent]):
        return POINTS["win"] + convert[me].value
    return POINTS["loose"] + convert[me].value


def game2(opponent: str, me: str) -> int:
    convert = {
        "A": rock,
        "X": "loose",
        "B": paper,
        "Y": "draw",
        "C": scissors,
        "Z": "win",
    }
    if convert[me] == "draw":
        return convert[opponent].draw.value + POINTS["draw"]
    if convert[me] == "loose":
        return convert[opponent].win.value + POINTS["loose"]
    if convert[me] == "win":
        return convert[opponent].loose.value + POINTS["win"]


def part1() -> int:
    """
    https://adventofcode.com/2022/day/2
    """
    input = file_input.read_pair_input(INPUT_NAME)
    return sum([game1(*i) for i in input])


def part2() -> int:
    """
    https://adventofcode.com/2022/day/2
    """
    input = file_input.read_pair_input(INPUT_NAME)
    return sum([game2(*i) for i in input])


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 13005


def test_part2():
    assert part2() == 11373


if __name__ == "__main__":
    print(part1())
    print(part2())
