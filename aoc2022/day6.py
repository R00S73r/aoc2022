from .lib import file_input
from collections import deque
from typing import Tuple

INPUT_NAME = "input6.txt"


def find_index(buffer: str, maxlen: int) -> int:
    indicator: deque = deque(maxlen=maxlen)
    for i, c in enumerate(buffer):
        indicator.append(c)
        if i + 1 >= maxlen and len(set(indicator)) == maxlen:
            return i + 1

    raise ValueError(f"No distinct frame with len {maxlen} found!")


def part1() -> int:
    """
    https://adventofcode.com/2022/day/6
    """
    datastream = file_input.read_input(INPUT_NAME)[0]
    return find_index(datastream, 4)


def part2() -> int:
    """
    https://adventofcode.com/2022/day/6
    """
    datastream = file_input.read_input(INPUT_NAME)[0]
    return find_index(datastream, 14)


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 1647


def test_part2():
    assert part2() == 2447


if __name__ == "__main__":
    print(part1())
    print(part2())
