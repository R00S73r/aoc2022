from __future__ import annotations
from .lib import file_input
from typing import Tuple, List
from operator import mul
from functools import reduce

INPUT_NAME = "input8.txt"


class Tree:
    def __init__(self, x: int, y: int, height: int) -> None:
        self.x = x
        self.y = y
        self.height = height

    def neighbors(self, top: Tree, right: Tree, bottom: Tree, left: Tree) -> None:
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left

    def is_edge(self) -> bool:
        return not bool(self.top and self.right and self.bottom and self.left)

    def visible_top(self, height: int) -> bool:
        return (
            self.is_edge() or self.top.height < height and self.top.visible_top(height)
        )

    def visible_bottom(self, height: int) -> bool:
        return (
            self.is_edge()
            or self.bottom.height < height
            and self.bottom.visible_bottom(height)
        )

    def visible_right(self, height: int) -> bool:
        return (
            self.is_edge()
            or self.right.height < height
            and self.right.visible_right(height)
        )

    def visible_left(self, height: int) -> bool:
        return (
            self.is_edge()
            or self.left.height < height
            and self.left.visible_left(height)
        )

    def is_visible(self) -> bool:
        return (
            self.visible_left(self.height)
            or self.visible_right(self.height)
            or self.visible_top(self.height)
            or self.visible_bottom(self.height)
        )

    def view_right(self, height) -> List[Tree]:
        if not self.right:
            return []
        if height <= self.right.height:
            return [self.right]
        return [self.right] + self.right.view_right(height)

    def view_left(self, height) -> List[Tree]:
        if not self.left:
            return []
        if height <= self.left.height:
            return [self.left]
        return [self.left] + self.left.view_left(height)

    def view_bottom(self, height) -> List[Tree]:
        if not self.bottom:
            return []
        if height <= self.bottom.height:
            return [self.bottom]
        return [self.bottom] + self.bottom.view_bottom(height)

    def view_top(self, height) -> List[Tree]:
        if not self.top:
            return []
        if height <= self.top.height:
            return [self.top]
        return [self.top] + self.top.view_top(height)

    def scenic_score(self) -> int:
        trees = [
            self.view_top(self.height),
            self.view_bottom(self.height),
            self.view_left(self.height),
            self.view_right(self.height),
        ]
        return reduce(mul, map(len, trees))

    def __repr__(self) -> str:
        return f"({self.x}/{self.y}) {self.height}"


def generate_forest(file_input: List[str]) -> List[List[Tree]]:
    grid = []
    for y, line in enumerate(file_input):
        grid.append([Tree(x, y, int(height)) for x, height in enumerate(line)])

    for y, line in enumerate(grid):
        for x, tree in enumerate(line):
            top = grid[y - 1][x] if y > 0 else None
            bottom = grid[y + 1][x] if y < len(grid) - 1 else None
            right = grid[y][x + 1] if x < len(line) - 1 else None
            left = grid[y][x - 1] if x > 0 else None
            tree.neighbors(top, right, bottom, left)
    return grid


def part1() -> int:
    """
    https://adventofcode.com/2022/day/8
    """
    forest = generate_forest(file_input.read_input(INPUT_NAME))
    return len([tree for row in forest for tree in row if tree.is_visible()])


def part2() -> int:
    """
    https://adventofcode.com/2022/day/8
    """
    forest = generate_forest(file_input.read_input(INPUT_NAME))
    return max([tree.scenic_score() for row in forest for tree in row])


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 1717


def test_part2():
    assert part2() == 321975


if __name__ == "__main__":
    print(part1())
    print(part2())
