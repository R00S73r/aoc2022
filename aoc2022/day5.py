from .lib import file_input
from typing import List, Dict, Tuple
from queue import LifoQueue
from collections import namedtuple

INPUT_NAME = "input5.txt"

Movement = namedtuple("Movement", ["amount", "from_", "to_"])


def split_input(file_input: List[str]) -> Tuple[List[str], List[str]]:
    first = []
    second = []
    first_part = True
    for line in file_input:
        if not line:
            first_part = False
            continue

        if first_part:
            first.append(line)
        else:
            second.append(line)
    return (first, second)


def get_stacks(file_input: List[str]) -> Dict[int, LifoQueue]:
    rows = [list(line) for line in file_input]
    stacks = {}
    rows.reverse()
    transpose = map(list, zip(*rows))
    for row in transpose:
        if row[0].isdigit():
            stack = LifoQueue()
            for i in range(1, len(row)):
                if not row[i].isspace():
                    stack.put(row[i])
            stacks[int(row[0])] = stack

    return stacks


def get_movements(file_input: List[str]) -> List[Movement]:
    only_num = [[int(c) for c in line.split() if c.isnumeric()] for line in file_input]
    return [Movement(amount, from_, to_) for amount, from_, to_ in only_num]


def move(
    stacks: Dict[int, LifoQueue], movement: Movement, same_order: bool = False
) -> None:
    if same_order:
        tmp = LifoQueue()
        for _ in range(movement.amount):
            elem = stacks[movement.from_].get_nowait()
            tmp.put_nowait(elem)
        for _ in range(movement.amount):
            elem = tmp.get_nowait()
            stacks[movement.to_].put_nowait(elem)
    else:
        for _ in range(movement.amount):
            elem = stacks[movement.from_].get_nowait()
            stacks[movement.to_].put_nowait(elem)


def get_top_lvl(stacks: Dict[int, LifoQueue]) -> str:
    top_lvl = [stack.get_nowait() for stack in stacks.values()]
    return "".join(top_lvl)


def part1() -> str:
    """
    https://adventofcode.com/2022/day/5
    """
    input = file_input.read_input(INPUT_NAME, empty_lines=True)
    starting_stacks, rearrangement = split_input(input)
    stacks = get_stacks(starting_stacks)
    movements = get_movements(rearrangement)
    for movement in movements:
        move(stacks, movement)
    return get_top_lvl(stacks)


def part2() -> str:
    """
    https://adventofcode.com/2022/day/5
    """
    input = file_input.read_input(INPUT_NAME, empty_lines=True)
    starting_stacks, rearrangement = split_input(input)
    stacks = get_stacks(starting_stacks)
    movements = get_movements(rearrangement)
    for movement in movements:
        move(stacks, movement, same_order=True)
    return get_top_lvl(stacks)


def solve() -> Tuple[str, str]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == "VPCDMSLWJ"


def test_part2():
    assert part2() == "TPWCGNCCG"


if __name__ == "__main__":
    print(part1())
    print(part2())
