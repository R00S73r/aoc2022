from .lib import file_input
from typing import Tuple, List
from collections import namedtuple

Range = namedtuple("Range", ("begin", "end"))

INPUT_NAME = "input4.txt"


def generate_ranges(file_input: List[Tuple[str, str]]) -> List[Tuple[set, set]]:
    ranges = [
        (Range(*range_a.split("-")), Range(*range_b.split("-")))
        for range_a, range_b in file_input
    ]
    full_ranges = []
    for range_a, range_b in ranges:
        full_range_a = set(range(int(range_a.begin), int(range_a.end) + 1))
        full_range_b = set(range(int(range_b.begin), int(range_b.end) + 1))
        full_ranges.append((full_range_a, full_range_b))
    return full_ranges


def part1() -> int:
    """
    https://adventofcode.com/2022/day/4
    """
    input = file_input.read_pair_input(INPUT_NAME, pivot=",")
    ranges = generate_ranges(input)
    subsets = []
    for range_a, range_b in ranges:
        if range_a <= range_b:
            subsets.append(range_a)
            continue
        if range_b <= range_a:
            subsets.append(range_b)
            continue
    return len(subsets)


def part2() -> int:
    """
    https://adventofcode.com/2022/day/4
    """
    input = file_input.read_pair_input(INPUT_NAME, pivot=",")
    ranges = generate_ranges(input)
    overlap = []
    for range_a, range_b in ranges:
        common = range_a & range_b
        if common:
            overlap.append(common)
    return len(overlap)


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 485


def test_part2():
    assert part2() == 857


if __name__ == "__main__":
    print(part1())
    print(part2())
