from .lib import file_input
from typing import Tuple
from string import ascii_letters

INPUT_NAME = "input3.txt"


def item_priority(item: str) -> int:
    return ascii_letters.index(item) + 1  # starts at 0


def split_content(ruckstack: str) -> Tuple[str, str]:
    n = len(ruckstack)
    if n % 2 != 0:
        raise ValueError("Only content with even length allowed")
    half = int(n / 2)
    return (ruckstack[0:half], ruckstack[half:n])


def part1() -> int:
    """
    https://adventofcode.com/2022/day/3
    """
    backpacks = file_input.read_input(INPUT_NAME)
    compartments = [split_content(backpack) for backpack in backpacks]
    common = [set(a) & set(b) for a, b in compartments]
    priorities = [item_priority(item) for items in common for item in items]
    return sum(priorities)


def part2() -> int:
    """
    https://adventofcode.com/2022/day/3
    """
    backpacks = file_input.read_input(INPUT_NAME)
    groups = [tuple(backpacks[i : i + 3]) for i in range(0, len(backpacks), 3)]
    common = [set(a) & set(b) & set(c) for a, b, c in groups]
    priorities = [item_priority(item) for items in common for item in items]
    return sum(priorities)


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 8085


def test_part2():
    assert part2() == 2515


if __name__ == "__main__":
    print(part1())
    print(part2())
