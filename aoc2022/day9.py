from __future__ import annotations
from .lib import file_input
from typing import Tuple, List
from dataclasses import dataclass
from math import sqrt

INPUT_NAME = "input9.txt"


@dataclass
class Movement:
    direction: str
    steps: int


@dataclass
class Point:
    x: int
    y: int

    def distance(self, other: Point) -> int:
        """
        https://en.wikipedia.org/wiki/Euclidean_distance
        """
        return int(sqrt(pow((self.x - other.x), 2) + pow((self.y - other.y), 2)))

    def __add__(self, step: Point) -> Point:
        self.x += step.x
        self.y += step.y
        return self

    def move(self, direction: str) -> None:
        directions = {
            "U": Point(0, 1),
            "R": Point(1, 0),
            "D": Point(0, -1),
            "L": Point(-1, 0),
        }
        self + directions[direction]

    def follow(self, to: Point) -> None:
        if self.distance(to) > 1:
            if self.x == to.x:
                self.move("U" if self.y < to.y else "D")
            elif self.y == to.y:
                self.move("R" if self.x < to.x else "L")
            else:
                self.move("R" if self.x < to.x else "L")
                self.move("U" if self.y < to.y else "D")

    @property
    def position(self) -> Tuple[int, int]:
        return (self.x, self.y)


def knot_movement(knots: List[Point], movement: Movement) -> List[Tuple[int, int]]:
    tail_positions = []
    for i in range(movement.steps):
        pivot = knots[0]
        pivot.move(movement.direction)
        for knot in knots[1:]:
            knot.follow(pivot)
            pivot = knot
        tail_positions.append(pivot.position)
    return tail_positions


def part1() -> int:
    """
    https://adventofcode.com/2022/day/9
    """
    movements = [
        Movement(line.split()[0], int(line.split()[-1]))
        for line in file_input.read_input(INPUT_NAME)
    ]
    knots = [Point(0, 0) for _ in range(2)]
    tail_positions = []
    for movement in movements:
        tail_positions += knot_movement(knots, movement)

    return len(set(tail_positions))


def part2() -> int:
    """
    https://adventofcode.com/2022/day/9
    """
    movements = [
        Movement(line.split()[0], int(line.split()[-1]))
        for line in file_input.read_input(INPUT_NAME)
    ]
    knots = [Point(0, 0) for _ in range(10)]
    tail_positions = []
    for movement in movements:
        tail_positions += knot_movement(knots, movement)

    return len(set(tail_positions))


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 6498


def test_part2():
    assert part2() == 2531


if __name__ == "__main__":
    print(part1())
    print(part2())
