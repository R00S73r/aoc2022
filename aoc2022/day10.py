from .lib import file_input
from typing import List, Tuple, Optional, Dict
from dataclasses import dataclass

INPUT_NAME = "input10.txt"
CIRCLES = {"addx": 2, "noop": 1}


@dataclass
class Instruction:
    operation: str
    value: Optional[int]
    cycles: int


def parse_instructions(code: List[str]) -> List[Instruction]:
    parsed = []
    for line in code:
        if " " in line:
            op, value = line.split()
            parsed.append(Instruction(op, int(value), CIRCLES[op]))
        else:
            op = line.strip()
            parsed.append(Instruction(op, None, CIRCLES[op]))
    return parsed


def generate_state(program: List[Instruction]) -> Dict[int, int]:
    state: Dict[int, int] = {}
    register = 1
    for instruction in program:
        for _ in range(1, instruction.cycles + 1):
            state[len(state) + 1] = register
        if instruction.value:
            register += instruction.value

    # additional cycle for last value to be applied
    state[len(state) + 1] = register
    return state


def part1() -> int:
    """
    https://adventofcode.com/2022/day/10
    """
    program = parse_instructions(file_input.read_input(INPUT_NAME))
    state = generate_state(program)
    return sum([state[cycle] * cycle for cycle in range(20, 260, 40)])


def part2() -> int:
    """
    https://adventofcode.com/2022/day/10
    """
    return 0


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 16480


def test_part2():
    assert part2() == 209691


if __name__ == "__main__":
    print(part1())
    print(part2())
