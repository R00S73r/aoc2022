from pathlib import Path
from typing import List, Tuple


def read_input(filename: str, empty_lines: bool = False) -> List[str]:
    """
    Read file with given name from input folder
    and return content as list of lines.
    """
    input_path = Path(__file__).parent.parent / "input" / filename
    with open(input_path) as fd:
        lines = [line.rstrip("\n") for line in fd.readlines()]
        if empty_lines:
            return lines
        return [line for line in lines if line]


def read_pair_input(filename: str, pivot: str = " ") -> List[Tuple]:
    """
    Read file with given name from input folder,
    split each line by given character into pairs and return them.
    """
    return [tuple(line.split(pivot)) for line in read_input(filename)]
