from __future__ import annotations
from .lib import file_input
from typing import Tuple, List, Optional, Union
from dataclasses import dataclass

INPUT_NAME = "input7.txt"


@dataclass
class File:
    size: int
    name: str


class Folder:
    def __init__(self, name: str, parent: Optional[Folder]) -> None:
        self.name = name
        self.parent: Optional[Folder] = parent
        self.files: List[File] = []
        self.childs: List[Folder] = []

    def add(self, obj: Union[File, Folder]) -> None:
        if isinstance(obj, File):
            self.files.append(obj)
        elif isinstance(obj, Folder):
            self.childs.append(obj)
        else:
            raise ValueError(f"Object from type '{type(obj)}' not supported!")

    @property
    def size(self) -> int:
        subfolder_size = sum([f.size for f in self.childs])
        return sum([f.size for f in self.files]) + subfolder_size

    def subfolders(self) -> List[str]:
        return [f.name for f in self.childs]

    def get_child(self, name: str) -> Folder:
        for f in self.childs:
            if f.name == name:
                return f
        raise ValueError(f"Subfolder '{name}' not found!")


def folder_sizes(filesystem: Folder) -> List[int]:
    total = [filesystem.size]
    for folder in filesystem.childs:
        total += folder_sizes(folder)
    return total


def parse(file_input: List[str]) -> Folder:
    file_tree = Folder("/", None)
    pointer = file_tree
    for line in file_input:

        if line.startswith("$ cd"):
            folder_name = line.lstrip("$ cd")
            if folder_name == "/":
                pointer = file_tree
            elif folder_name == "..":
                assert pointer.parent, f"Already on top: {pointer.name}"
                pointer = pointer.parent
            elif folder_name in pointer.subfolders():
                pointer = pointer.get_child(folder_name)
            else:
                new_folder = Folder(folder_name, pointer)
                pointer.add(new_folder)
                pointer = new_folder

        if not line.startswith("$"):
            if line.startswith("dir"):
                folder_name = line.lstrip("dir ")
                new_folder = Folder(folder_name, pointer)
                pointer.add(new_folder)
            else:  # 23453 b.txt
                size, name = line.split()
                pointer.add(File(int(size), name))

    return file_tree


def part1() -> int:
    """
    https://adventofcode.com/2022/day/7
    """
    console_output = file_input.read_input(INPUT_NAME)
    filesystem = parse(console_output)
    return sum([size for size in folder_sizes(filesystem) if size <= 100000])


def part2() -> int:
    """
    https://adventofcode.com/2022/day/7
    """
    console_output = file_input.read_input(INPUT_NAME)
    filesystem = parse(console_output)
    unused_space = 70000000 - filesystem.size
    space_needed = 30000000 - unused_space
    return sorted([size for size in folder_sizes(filesystem) if size > space_needed])[0]


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 1348005


def test_part2():
    assert part2() == 12785886


if __name__ == "__main__":
    print(part1())
    print(part2())
