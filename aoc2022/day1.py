from .lib import file_input
from typing import List, Tuple

INPUT_NAME = "input1.txt"


def sum_till_emtpy(input: List[str]) -> List[int]:
    tmp = 0
    sums = []
    for line in input:
        if line:
            tmp += int(line)
        if not line:
            sums.append(tmp)
            tmp = 0
    sums.append(tmp)  # last item
    return sums


def part1() -> int:
    """
    https://adventofcode.com/2022/day/1
    """
    input = file_input.read_input(INPUT_NAME, empty_lines=True)
    return max(sum_till_emtpy(input))


def part2() -> int:
    """
    https://adventofcode.com/2022/day/1
    """
    input = file_input.read_input(INPUT_NAME, empty_lines=True)
    sums = sum_till_emtpy(input)
    sums.sort(reverse=True)
    return sum(sums[0:3])


def solve() -> Tuple[int, int]:
    """
    For external integration.
    """
    return (part1(), part2())


def test_part1():
    assert part1() == 71300


def test_part2():
    assert part2() == 209691


if __name__ == "__main__":
    print(part1())
    print(part2())
